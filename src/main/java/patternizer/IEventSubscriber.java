package patternizer;

public interface IEventSubscriber {
	public void handleEvent(Object event);
}
