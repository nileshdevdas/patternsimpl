package patternizer;

public interface IEventPublisher {

	public void publish(Object object);

}
