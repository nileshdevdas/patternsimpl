package patternizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class HeavyWorkImpl {
	private static final Logger logger = LoggerFactory.getLogger(HeavyWorkImpl.class);

	public HeavyWorkImpl() {
		logger.info("HeavyWorkImpl::constructor");
	}
}
