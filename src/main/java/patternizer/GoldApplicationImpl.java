package patternizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <singleton name="abc"> <scans> <scan basePackage="test"></scan>
 * <scan basePackage="com"></scan> </scans> </singleton>
 * 
 * @author niles
 *
 */
@Singleton(name = "goldApp")
public class GoldApplicationImpl implements IApplication, LifecycleListener, IEventSubscriber {
	private static final Logger logger = LoggerFactory.getLogger(GoldApplicationImpl.class);

	@Override
	public void init() {
		logger.info("GoldApplicationImpl::init");
	}

	@Override
	public void destroy() {
		logger.info("GoldApplicationImpl::destroy");

	}

	public GoldApplicationImpl() {
		logger.info("GoldApplicationImpl::constructor");
	}

	@Override
	public void deposit() {
	}

	@Override
	public void withdraw() {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleEvent(Object event) {
		logger.info("Recevied event ", event);
	}
}
