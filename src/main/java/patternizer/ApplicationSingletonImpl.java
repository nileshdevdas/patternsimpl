package patternizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class ApplicationSingletonImpl implements LifecycleListener {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationSingletonImpl.class);

	@Override
	public void init() {
		logger.info("ApplictionSingleon::init");
	}

	@Override
	public void destroy() {
		logger.info("ApplictionSingleon::destroy");
	}
}
