package patternizer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerAdvice implements InvocationHandler {
	private Logger logger = LoggerFactory.getLogger(LoggerAdvice.class);

	private Object target = null;

	public LoggerAdvice(Object target) {
		this.target = target;
	}

	public void entry(Method method) {
		logger.info("Entered ::: " + method.getDeclaringClass() + "::" + method.getName());
	}

	public void exit(Method method) {
		logger.info("Exited ::: " + method.getDeclaringClass() + "::" + method.getName());
	}

	public void fault(Throwable e) {
		logger.error(e.getMessage());
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		try {
			entry(method); // before
			result = method.invoke(target, args);
			exit(method);// after
		} catch (Exception e) {
			fault(e); // throws
			throw e;
		}
		return result;
	}
}
