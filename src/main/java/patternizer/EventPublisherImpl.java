package patternizer;

import java.util.Map;

public class EventPublisherImpl implements IEventPublisher {

	private Map<String, Object> instances = null;

	public EventPublisherImpl(Map<String, Object> instances) {
		this.instances = instances;
	}

	@Override
	public void publish(Object object) {
		// write your code for publishing event
	}

}
