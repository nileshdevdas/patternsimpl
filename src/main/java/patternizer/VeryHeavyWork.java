package patternizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class VeryHeavyWork {
	private static final Logger logger = LoggerFactory.getLogger(VeryHeavyWork.class);

	public VeryHeavyWork() {
		logger.info("VeryHeavyWork::constructor");
	}
}
