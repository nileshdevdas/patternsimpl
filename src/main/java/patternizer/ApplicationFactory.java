package patternizer;

import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

@Scan(basePackage = "patternizer")
public class ApplicationFactory {
	private static final Map<String, Object> singletons = new HashMap<>();
	private static Reflections reflections = null;

	private ApplicationFactory() {
	}

	/**
	 * LifeCyle Management All the Object That get Created in the Factory
	 */
	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				singletons.keySet().forEach((each) -> {
					if (singletons.get(each) instanceof LifecycleListener) {
						LifecycleListener lifecycle = (LifecycleListener) singletons.get(each);
						lifecycle.destroy();
					}
				});
			}
		});
		intializeSingleTons();
	}

	public static void publishEvent() {

	}

	private static void intializeSingleTons() {
		reflections = new Reflections("patternizer", new SubTypesScanner(false), new TypeAnnotationsScanner());
		reflections.getTypesAnnotatedWith(Singleton.class).forEach((each) -> {
			if (!Modifier.isAbstract(each.getModifiers())) {
				try {
					Singleton sg = each.getAnnotation(Singleton.class);
					if (!sg.lazy()) {
						Object obj = each.getDeclaredConstructor().newInstance();
						Class<?> ifs[] = each.getInterfaces();
						for (int i = 0; i < ifs.length; i++) {
							singletons.put(ifs[i].getName(), obj);
						}
						handleLifeCycleInit(obj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static Object getInstance(Class<?> clz) {
		Object object = null;
		try {
			if (singletons.containsKey(clz.getName())) {
				object = singletons.get(clz.getName());
			} else {
				handleInstantiation(clz);
			}
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
		// Dynamic Weaving of Application code...

		return Proxy.newProxyInstance(ApplicationFactory.class.getClassLoader(), new Class[] { IApplication.class },
				new LoggerAdvice(object));
	}

	private static Object handleInstantiation(Class clz) throws Throwable {
		Object object = null;
		Set<Class<?>> subtypes = reflections.getSubTypesOf(clz);
		Class<?> instantiable = subtypes.stream().findFirst().orElseThrow();
		if (instantiable.isAnnotationPresent(Singleton.class)) {
			object = instantiable.getDeclaredConstructor().newInstance();
			singletons.put(clz.getName(), object);
		} else {
			object = instantiable.getDeclaredConstructor().newInstance();
		}
		handleLifeCycleInit(object);
		return object;
	}

	private static void handleLifeCycleInit(Object object) {
		if (object instanceof LifecycleListener) {
			LifecycleListener lifecycle = (LifecycleListener) object;
			lifecycle.init();
		}
	}
}
