package patternizer;

public interface LifecycleListener {
	void init();

	void destroy();
}
