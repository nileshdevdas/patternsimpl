package patternizer;

/**
 * A interface that deals with the business functionality
 * 
 * @author niles
 *
 */
public interface IApplication {
	void deposit();

	void withdraw();
}
